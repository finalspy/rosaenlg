import { getDistanceReport } from './distanceReport';
import { getDistancePourcentage, getDistanceRaw } from './distance';
import { getClusters } from './cluster';

export = {
  getDistanceReport: getDistanceReport,
  getDistancePourcentage: getDistancePourcentage,
  getDistanceRaw: getDistanceRaw,
  getClusters: getClusters,
};

# RosaeNLG node.js Server

RosaeNLG is a Natural Language Generation library.
This is a node.js server around [RosaeNLG](https://rosaenlg.org).

For documentation, see [RosaeNLG main documentation](https://rosaenlg.org).

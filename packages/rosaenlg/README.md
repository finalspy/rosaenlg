# RosaeNLG

RosaeNLG is a Natural Language Generation library for node.js or client side (browser) execution, based on the [Pug](https://pugjs.org/) template engine.

Documentation is automatically published here: https://rosaenlg.org
